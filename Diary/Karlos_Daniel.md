This diary file is written by Karlos Daniel E14085147 in the course Professional skills for engineering the third industrial revolution.

# 2021-09-23 #
* In the second lecture, the Professor explain more about conflict, that the conflict are divided into a several level. 
* I can learn how to use bitbucket, even though I have a little bit problem to use it for the first time.
* From the TED talk video by Steven Pinker about "Is the world getting better or worse? A look at the numbers". I got that, the world will getting better in the future.

# 2021-09-30 #
* This week lecture is about fake news.
* learn how to spot a bad statistic. (3 ways: 1. Do you see uncertainty in the data? 2. Do you see yourself in the data? 3. How was the data collected?)
* I feel Mona Chalabi talk too fast, so its hard to understand what she saying. but in the end I got the point.
* My team got a chance to present this week.

# 2021-10-07 #
* This week lecture is about money (economy and finance).
* Money and credit are used for trading or transactions. (consist buyers and seller)
* Learned about how money was determined, the value of money, and about economic growth.
* Filling a questionnaire about money and economy in the world.
* Having a new group for presentation starting from next week.

# 2021-10-14 #
* From today presentation, I learn about the finance from different country which is Taiwan, Malaysia, and Thailand.
* I think making video to presentation it will give us more chance to get score.
* learn more about nationalism and how important it is.
* learn about facism that I never heard before.

# 2021-10-21 #
* In this week, I got chance to read my diary.
* This week lesson is about anxiety and healhty life.
* Learn how anxiety can affect our heart rate and the way we thinking.
* When under pressure, we can't think clearly.

# 2021-10-28 #
* This week, we are change a new group again and create a supergroup.
* Did a supergroup discussion today, but the group was silence and in the end we were late for presentation.
* From the supergroup presentation, knowing that if drink too much water can cause death.
* I think mental illnesses such as depression is a little scary because we cannot determine the damage caused by it.
* In serious cases, depression may cause us to suicide. Even the patient who are suffered did now want to do that, but they are just being manipulated by their emotions at the time.
* I think the best medicine for depression people is someone to talk to. In order word, we need to let them feel society and don't let them feel lonely.
