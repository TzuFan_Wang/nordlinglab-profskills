*This diary file is written by Shu-Wen Guo (郭姝彣) E54091112


# 2021-09-30

* It was my first time in this class. As a result of not attending the the first two lectures, I have some difficulty in understanding what's going on; also,
 attending a all-english class isn;t a piece of cake for me, but I think I'll get better gradually.
* I think this class would be a lot better if it is physical. Hopefully we don't have to take online classes whole semester.
* This website is hard to use. It will be a lot simpler if we just use the website NCKU provides.
* From https://newsvoice.se/2021/10/jason-christoff-mental-conditioning/, I learn more about news and ways to avoid taking in fake news.

# 2021-10-07

* I delivered a presentation today. Giving a presentation in a foreign language isn't easy for me; I spent a lot more time to prepare my text of speech than normal.
 Nevertheless, the result is good. I got the chance the show my work, and the presentation went out nicely.
* The topic of finance in this lecture is too hard for me, and the content seemed too much, since we're not able to watch those videos in class. However, this is
 still an nice lecture, I learn some interesting things; for example, how to print a million NTD legally.
 
# 2021-10-14

* I am glad that diary reading wasn't included in this week's lecture. Don't take me wrong. I don't hate hearing people sharing their diaries. However, I do think
  it is taking up too much time.
* The videos we are required to watch is interesting. I find it a little stupid how we humans live in fictions that we create, but it is also fun that we create
  myths for explaining the unknown, and rules for maintaining social orders.
  
# 2021-10-21

* These TEDTALKs are all informative, but among them I love the presentation that Wendy Suzuki gave. It was very fun, and I can totally felt her enthusiasm.
 I especially admire her including her own experience in the speech. It make it more relatable. Last but not least, the exercise she made the participants did at
  the end just gave the speech a perfect ending. I learn a lot from this video; not only the knowledge about how exercise can affect brain, but also tips for
  delivering a nice presentation.
  
# 2021-10-28

* My network is down in the middle of the class and I failed to catch up afterward. As a result, sadly, I didn't know what happened after we were divided into 
 supergroups.
