







# 2021-10-10 #

* I finally created my git diary after many failed attempts (and writing on my own repository for 2 weeks)
* One of my friends got kidney stones because of drinking too much coke, I think I'll stop drinking it after that
* This class was interesting but I still got distracted all the time, my medicine is not working as good as it used to.
* I bought a Switch so I can relax and play between classes, the videos the professor showed us are really interesting to watch outside of class, but they're a bit distracting for me during class (I end up watching other videos)

# 2021-10-21 #	

* This class was quite interesting, it touched some topics that really interest me.
* My meds don't work at all and I still get distracted during calss.
* I forgot to talk to my teammates after I contacted them.
* The videos are interesting but sometimes the timing of the class gets messed up by the videos.

# 2021-10-28 #

* This class was really interesting as it touched few themes that I'm familiar with. 
* I was diagnosed with depression and ADHD a few years ago and this class gave me some insight on my own feelings of anxiety. 
* Around 80% of engineering students have depression or anxiety..
* The future of the earth and climate change dworsen my feelings and generate some anxiety. 