This diary file is written by Andy Wu E24085327 in the course Professional skills for engineering the third industrial revolution.

# 2021-09-30 #

* I've decided to continue on this course since it helped me improve my researching skill and get to understand what's going on in the world.
* For my group's previous presentation, I've learned not to give credits or mention a member's name if the person didn't contribute and is no longer a part of our group.
* I hope that this class would inspire me to see the opportunity and pathway to make in the future's society.

# 2021-10-7 #

* Money types I learned are officially issued legal tender or fiat-types, representative, and even bit-coin.
* Printing more money during Chinese new year causes price to increase.
* I learned that when we take a loan from the bank, money will be created and not borrowed from other people's account.

# 2021-10-14 #

* I like the idea of professor giving students the opportunity to get presentation points by creating videos instead of presenting directly in class.
* I hope that everyone would get the minimum points for the presentation. When everyone gets their minimum score, students who volunteer to present more will get extra points.
* Because there are less people who want to share their dairy, maybe those who volunteer can get extra presentation points.

# 2021-10-21 #

* I agree with improving health care instead of sick care. Why not take care of yourself to live healthier and longer instead of treatment after the discovery of one's sickness?
* Exercising 30 minutes each day helps improve not only your physical health but also your mental health.
* I will consider in changing my university lifestyle in terms of performing other activities, such as exercising and socializing, instead of studying too much each day.

# 2021-10-28 #

* We had to form a supergroup with 3 groups of 3 people, which there were about 9 people in the group. It was interesting to see how we interact with others by having online discussion because it was way different from having face to face conversation.
* We all face the conflict within ourselves, such as having anxiety and depression. Even though it is natural for us to feel those negative emotions, but I also believe that our genetics plays a role in the way we control our mental health.
* There's a popular point of view on suicide, which is "suicide affects people around you". Some people, like me, agree with the quote, but some feels if the quote is terrible as it means people only care about you when you aren't here anymore, and others are selfish in thinking they are more important than those who have suicidal thoughts.

