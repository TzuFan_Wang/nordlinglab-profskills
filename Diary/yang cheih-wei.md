# 2021-09-30

* Trying to get to know the team members and the cooperation goes well
* I prepared a briefing, but unfortunately I was not selected by the professor
* I found that the briefing was not as simple as I imagined

# 2021-10-07

* Although I was still not selected for the presentation, I still learned a lot from others
* Know more presentation skills and also understand my own shortcomings

# 2021-10-14

* Finding information for the presentation is more difficult than I thought
* I didn’t study the economy, so it’s very new to me
* I didn’t know anything about economics, so it’s quite new to me
* Finding information for the presentation is more difficult than I thought
