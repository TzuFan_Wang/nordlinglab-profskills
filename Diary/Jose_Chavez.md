This diary file is written by Jose Chavez (E24057081) for the course of Professional Skills for Engineering the Third Industrial Revolution.

# 2019-09-26
* Explanation on how to write the diary
* Presentations of mini problem 2: Claim by Steven Pinker.
* TED Talk: How to seek truth in the era of fake news(by Christiane Amanpour).
* Introduction about how to cite articles and copyright of material on websites.
* TED Talk: Inside the fight against Russia's fake news empire(by Olga Yurkova).
* Detect fake news by checking if what, why, when, how, where, and who are answered.

# 2019-10-03
* Classmates share their diary with the class.
* Presentation of mini problem 3: Claim of fake news.
* Video explanation on how the banking system and financial sector actually work by Prof. Werner.
* Video how the economic machine works by Ray Dalio.
* Video the $10,000 experiment by Adam Carroll.
* Discussion about critical thinking after watching videos related to economy.

# 2019-11-07
* Today the class started with a very strong image about living people doing fake funerals and I believe, as professor said, it may work for some but not for everyone and I agree because some people could get the wrong idea
* When we watched the second video about forgiveness I was very amazed about how this man forgave the person that ended his son’s life, I personally would have a very difficult time with forgiven someone that did something that tragic
* In today’s class we found two claims: 1. “the Tariq Khamisa Foundation was able to cut suspensions and expulsions by 70 percent trough the safe school model” 2. “the suicide rate has been rising around the world, and it recently reached a 30-year high in America”

# 2019-11-14
* One classmate shares his diary to whole class.
* Presentation of mini problem: Claim and Hypothesis; Three tangible rules for how to live a happy and fulfilling life.
* Video watching about how does the legal system work.
* I am not quite familar with the domain knowledge and terms of legal dictionary in English; therefore, it's a little bit difficult for me to get whole ideas in the video.
* I volunteered to present about the difference between the French legal system and U.K's legal system

# 2019-11-21
* Presentation of mini problem: comparison between the english and french legal systems
* Watching videos about big data and the rol of social networks in today's society
* Formed new gropus and discussed with the class the topic for the next lecture
* Formed a big group with the rest of the class to present ideas for next week's mini problem